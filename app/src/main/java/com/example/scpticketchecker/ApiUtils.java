package com.example.scpticketchecker;

import Interface.JsonAddOceanApi;

public class ApiUtils {

    private ApiUtils(){

    }

    public static final String BASE_URL = "http://localhost:8080";

    public static JsonAddOceanApi getAPIService(){
        return RetrofitClient.getClient(BASE_URL).create(JsonAddOceanApi.class);
    }



}
