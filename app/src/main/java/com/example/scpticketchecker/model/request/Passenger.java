package com.example.scpticketchecker.model.request;

import java.util.Date;

public class Passenger {

    private String ticketNumber;
    private String name;
    private String surname;
    private String ageCategory;
    private String gender;
    private String birthDate;
    private String nationality;
    private String documentType;
    private String passportNumber;
    private String passportExpDate;
    private String specialServicesReq;
    private String accomodationType;
    private String cabinSeatNumber;
    private Date berth;
    private String boardingCardNumber;
    private String typeOfProofOfResidence;
    private String military;
    private String largeFamily;

    public Passenger(String ticketNumber, String name, String surname, String ageCategory, String gender, String birthDate, String nationality, String documentType, String passportNumber, String passportExpDate, String specialServicesReq, String accomodationType, String cabinSeatNumber, Date berth, String boardingCardNumber, String typeOfProofOfResidence, String military, String largeFamily) {
        this.ticketNumber = ticketNumber;
        this.name = name;
        this.surname = surname;
        this.ageCategory = ageCategory;
        this.gender = gender;
        this.birthDate = birthDate;
        this.nationality = nationality;
        this.documentType = documentType;
        this.passportNumber = passportNumber;
        this.passportExpDate = passportExpDate;
        this.specialServicesReq = specialServicesReq;
        this.accomodationType = accomodationType;
        this.cabinSeatNumber = cabinSeatNumber;
        this.boardingCardNumber = boardingCardNumber;
        this.typeOfProofOfResidence = typeOfProofOfResidence;
        this.military = military;
        this.largeFamily = largeFamily;
        this.berth = berth;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAgeCategory() {
        return ageCategory;
    }

    public void setAgeCategory(String ageCategory) {
        this.ageCategory = ageCategory;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportExpDate() {
        return passportExpDate;
    }

    public void setPassportExpDate(String passportExpDate) {
        this.passportExpDate = passportExpDate;
    }

    public String getSpecialServicesReq() {
        return specialServicesReq;
    }

    public void setSpecialServicesReq(String specialServicesReq) {
        this.specialServicesReq = specialServicesReq;
    }

    public String getAccomodationType() {
        return accomodationType;
    }

    public void setAccomodationType(String accomodationType) {
        this.accomodationType = accomodationType;
    }

    public String getCabinSeatNumber() {
        return cabinSeatNumber;
    }

    public void setCabinSeatNumber(String cabinSeatNumber) {
        this.cabinSeatNumber = cabinSeatNumber;
    }

    public String getBoardingCardNumber() {
        return boardingCardNumber;
    }

    public void setBoardingCardNumber(String boardingCardNumber) {
        this.boardingCardNumber = boardingCardNumber;
    }

    public String getTypeOfProofOfResidence() {
        return typeOfProofOfResidence;
    }

    public void setTypeOfProofOfResidence(String typeOfProofOfResidence) {
        this.typeOfProofOfResidence = typeOfProofOfResidence;
    }

    public String getMilitary() {
        return military;
    }

    public void setMilitary(String military) {
        this.military = military;
    }

    public String getLargeFamily() {
        return largeFamily;
    }

    public void setLargeFamily(String largeFamily) {
        this.largeFamily = largeFamily;
    }

    public Date getBerth() {
        return berth;
    }

    public void setBerth(Date berth) {
        this.berth = berth;
    }
}
