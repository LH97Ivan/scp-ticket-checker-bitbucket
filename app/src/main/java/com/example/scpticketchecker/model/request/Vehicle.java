package com.example.scpticketchecker.model.request;

public class Vehicle {

    private String boardingCardNumber;
    private String ticketNumber;
    private String vehicleType;
    private String registrationNumber;
    private String driverName;
    private String brand;
    private String model;

    public Vehicle(String boardingCardNumber, String ticketNumber, String vehicleType, String registrationNumber, String driverName, String brand, String model) {
        this.boardingCardNumber = boardingCardNumber;
        this.ticketNumber = ticketNumber;
        this.vehicleType = vehicleType;
        this.registrationNumber = registrationNumber;
        this.driverName = driverName;
        this.brand = brand;
        this.model = model;
    }

    public String getBoardingCardNumber() {
        return boardingCardNumber;
    }

    public void setBoardingCardNumber(String boardingCardNumber) {
        this.boardingCardNumber = boardingCardNumber;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
