package com.example.scpticketchecker.model.request;

import java.util.List;

public class BookingInfoData {

    private String bookingNumber;
    private String actualCheckingDate;
    private String actualCheckingTime;

    private List<Passenger> passengers;
    private List<Vehicle> vehicles;

    public BookingInfoData(String bookingNumber, String actualCheckingDate, String actualCheckingTime) {
        this.bookingNumber = bookingNumber;
        this.actualCheckingDate = actualCheckingDate;
        this.actualCheckingTime = actualCheckingTime;
    }

    public String getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public String getActualCheckingDate() {
        return actualCheckingDate;
    }

    public void setActualCheckingDate(String actualCheckingDate) {
        this.actualCheckingDate = actualCheckingDate;
    }

    public String getActualCheckingTime() {
        return actualCheckingTime;
    }

    public void setActualCheckingTime(String actualCheckingTime) {
        this.actualCheckingTime = actualCheckingTime;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }
}
