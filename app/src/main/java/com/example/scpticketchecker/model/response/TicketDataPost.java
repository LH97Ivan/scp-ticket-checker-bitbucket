package com.example.scpticketchecker.model.response;

import com.example.scpticketchecker.Enums.CompanyEnum;
import com.example.scpticketchecker.Enums.ElementTypeEnum;

public class TicketDataPost {

    private CompanyEnum company;
    private ElementTypeEnum elementType;
    private String number;
    private Integer precheckerResult;
    private Long milliesOfResponse;

    public TicketDataPost(CompanyEnum company, ElementTypeEnum elementType, String number, Integer precheckerResult, Long milliesOfResponse) {
        this.company = company;
        this.elementType = elementType;
        this.number = number;
        this.precheckerResult = precheckerResult;
        this.milliesOfResponse = milliesOfResponse;
    }

    public TicketDataPost(){
        this.precheckerResult = 0000;
    }

    public CompanyEnum getCompany() {
        return company;
    }

    public void setCompany(CompanyEnum company) {
        this.company = company;
    }

    public ElementTypeEnum getElementType() {
        return elementType;
    }

    public void setElementType(ElementTypeEnum elementType) {
        this.elementType = elementType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getPrecheckerResult() {
        return precheckerResult;
    }

    public void setPrecheckerResult(Integer precheckerResult) {
        this.precheckerResult = precheckerResult;
    }

    public Long getMilliesOfResponse() {
        return milliesOfResponse;
    }

    public void setMilliesOfResponse(Long milliesOfResponse) {
        this.milliesOfResponse = milliesOfResponse;
    }
}
