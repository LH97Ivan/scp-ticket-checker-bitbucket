package com.example.scpticketchecker.model.request;

public class ShippingLineStatus {

    private boolean amlStatus;
    private boolean baleariaStatus;
    private boolean frsStatus;
    private boolean intershippingStatus;
    private boolean trasmediterraneaStatus;

    public ShippingLineStatus(){
    }

    public boolean getAmlStatus() {
        return amlStatus;
    }

    public void setAmlStatus(boolean amlStatus) {
        this.amlStatus = amlStatus;
    }

    public boolean getBaleariaStatus() {
        return baleariaStatus;
    }

    public void setBaleariaStatus(boolean baleariaStatus) {
        this.baleariaStatus = baleariaStatus;
    }

    public boolean getFrsStatus() {
        return frsStatus;
    }

    public void setFrsStatus(boolean frsStatus) {
        this.frsStatus = frsStatus;
    }

    public boolean getIntershippingStatus() {
        return intershippingStatus;
    }

    public void setIntershippingStatus(boolean intershippingStatus) {
        this.intershippingStatus = intershippingStatus;
    }

    public boolean getTrasmediterraneaStatus() {
        return trasmediterraneaStatus;
    }

    public void setTrasmediterraneaStatus(boolean trasmediterraneaStatus) {
        this.trasmediterraneaStatus = trasmediterraneaStatus;
    }
}
