package com.example.scpticketchecker.model.request;

public class TicketData {

    private String departureDate;
    private String departureTime;
    private String departurePort;
    private String arrivalPort;
    private String vesselIMO;
    private String vesselName;
    private String issuingCompanyCode;
    private String checkingCompanyCode;
    private String boardingCompanyCode;
    private Integer totalPassengers;
    private Integer totalVehicles;

    private BookingInfoData bookingInfoData;

    public TicketData(){

    }

    public TicketData(String departureDate, String departureTime, String departurePort, String arrivalPort, String vesselIMO, String vesselName, String issuingCompanyCode, String checkingCompanyCode, String boardingCompanyCode, Integer totalPassengers, Integer totalVehicles) {
        this.departureDate = departureDate;
        this.departureTime = departureTime;
        this.departurePort = departurePort;
        this.arrivalPort = arrivalPort;
        this.vesselIMO = vesselIMO;
        this.vesselName = vesselName;
        this.issuingCompanyCode = issuingCompanyCode;
        this.checkingCompanyCode = checkingCompanyCode;
        this.boardingCompanyCode = boardingCompanyCode;
        this.totalPassengers = totalPassengers;
        this.totalVehicles = totalVehicles;
    }


    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getDeparturePort() {
        return departurePort;
    }

    public void setDeparturePort(String departurePort) {
        this.departurePort = departurePort;
    }

    public String getArrivalPort() {
        return arrivalPort;
    }

    public void setArrivalPort(String arrivalPort) {
        this.arrivalPort = arrivalPort;
    }

    public String getVesselIMO() {
        return vesselIMO;
    }

    public void setVesselIMO(String vesselIMO) {
        this.vesselIMO = vesselIMO;
    }

    public String getVesselName() {
        return vesselName;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getIssuingCompanyCode() {
        return issuingCompanyCode;
    }

    public void setIssuingCompanyCode(String issuingCompanyCode) {
        this.issuingCompanyCode = issuingCompanyCode;
    }

    public String getCheckingCompanyCode() {
        return checkingCompanyCode;
    }

    public void setCheckingCompanyCode(String checkingCompanyCode) {
        this.checkingCompanyCode = checkingCompanyCode;
    }

    public String getBoardingCompanyCode() {
        return boardingCompanyCode;
    }

    public void setBoardingCompanyCode(String boardingCompanyCode) {
        this.boardingCompanyCode = boardingCompanyCode;
    }

    public Integer getTotalPassengers() {
        return totalPassengers;
    }

    public void setTotalPassengers(Integer totalPassengers) {
        this.totalPassengers = totalPassengers;
    }

    public Integer getTotalVehicles() {
        return totalVehicles;
    }

    public void setTotalVehicles(Integer totalVehicles) {
        this.totalVehicles = totalVehicles;
    }

    public BookingInfoData getBookingInfoData() {
        return bookingInfoData;
    }

    public void setBookingInfoData(BookingInfoData bookingInfoData) {
        this.bookingInfoData = bookingInfoData;
    }
}
