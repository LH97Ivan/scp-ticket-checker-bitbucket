package com.example.scpticketchecker;

import android.view.animation.Animation;
import androidx.annotation.DrawableRes;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ToggleButton {

    private boolean state;
    private boolean enabled;
    private int clickImageId;
    private int unclickImageId;
    private int disableImageId;
    private FloatingActionButton fab;

    public ToggleButton(FloatingActionButton fab) {
        this.fab = fab;
        fab.setEnabled(false);
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
        refresh();
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        fab.setEnabled(enabled);
        this.state = false;
        refresh();
    }

    public void setVisibility(int visibility) {
        fab.setVisibility(visibility);
    }

    public void startAnimation(Animation animation){
        fab.startAnimation(animation);
    }

    public void isClickable(boolean value){
        fab.setClickable(value);
    }

    public void setClickImage(@DrawableRes int resId) {
        clickImageId = resId;
    }

    public void setUnclickImage(@DrawableRes int resId) {
        unclickImageId = resId;
    }

    public void setDisableImageId(@DrawableRes int resId) {
        this.disableImageId = resId;
    }

    public void refresh() {
        if (enabled) {
            if (state) {
                this.fab.setImageResource(clickImageId);
            } else {
                this.fab.setImageResource(unclickImageId);
            }
        } else {
            this.fab.setImageResource(disableImageId);
        }

    }
}
