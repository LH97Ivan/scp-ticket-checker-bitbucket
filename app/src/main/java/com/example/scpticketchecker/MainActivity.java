package com.example.scpticketchecker;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import com.example.scpticketchecker.Enums.CompanyEnum;
import com.example.scpticketchecker.Enums.ElementTypeEnum;
import com.example.scpticketchecker.model.request.Passenger;
import com.example.scpticketchecker.model.request.ShippingLineStatus;
import com.example.scpticketchecker.model.request.TicketData;
import com.example.scpticketchecker.model.request.Vehicle;
import com.example.scpticketchecker.model.response.TicketDataPost;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import Interface.JsonAddOceanApi;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private Animation fromLeft;
    private Animation toLeft;

    private EditText et1;
    private Button buttonIcon1;
    private Button buttonIcon2;
    private Button buttonIcon3;
    private Button buttonIcon4;
    private Button buttonIcon5;
    private Button buttonComp1;
    private Button buttonComp2;
    private Button buttonComp3;

    private String selectedCompanyName;
    private String selectedNumber;
    private String selectedTicketType;

    private TextView resultTextView;
    private LinearLayout resultLayout;

    private ToggleButton greenButton;
    private ToggleButton quantityButton;
    private ToggleButton vehicleButton;
    private ToggleButton ageButton;
    private ToggleButton sendButton;
    private ToggleButton boardingButton;
    private ToggleButton expandableButton;

    private boolean expandableClicked = false;

    private String ticketNotFound = "Ticket not found";

    private TicketDataPost ticketDataPost;
    private JsonAddOceanApi mAPIService;

    private ProgressBar progressBar;

    private boolean arrivalPortTanger = false;
    private String arrivalPortTangerString = "arrivalPortTanger";

    private PrecheckGrabbaDriver precheckGrabbaDriver;

    private Map<String, Boolean> post = new HashMap<>();

    private Date beforeAnswer;
    private Date afterAnswer;
    private Long diffInMillies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scp_main);

        precheckGrabbaDriver = new PrecheckGrabbaDriver(this);

        mAPIService = ApiUtils.getAPIService();

        fromLeft = AnimationUtils.loadAnimation(this, R.anim.from_left_anim);
        toLeft = AnimationUtils.loadAnimation(this, R.anim.to_left_anim);

        et1 = (EditText) findViewById(R.id.editText1);
        buttonIcon1 = findViewById(R.id.button1);
        buttonIcon2 = findViewById(R.id.button2);
        buttonIcon3 = findViewById(R.id.button3);
        buttonIcon4 = findViewById(R.id.button4);
        buttonIcon5 = findViewById(R.id.button5);
        buttonComp1 = findViewById(R.id.button6);
        buttonComp2 = findViewById(R.id.button7);
        buttonComp3 = findViewById(R.id.button8);

        selectedCompanyName = "";
        selectedNumber = "";
        selectedTicketType = "";

        resultTextView = (TextView) findViewById(R.id.resultTextView);
        resultLayout = (LinearLayout) findViewById(R.id.layout5);

        boolean amlPost = Boolean.parseBoolean(Helper.getConfigValue(this, "post.AML"));
        boolean baleariaPost = Boolean.parseBoolean(Helper.getConfigValue(this, "post.BALEARIA"));
        boolean frsPost = Boolean.parseBoolean(Helper.getConfigValue(this, "post.FRS"));
        boolean interShippingPost = Boolean.parseBoolean(Helper.getConfigValue(this, "post.INTERSHIPPING"));
        boolean trasmediterraneaPost = Boolean.parseBoolean(Helper.getConfigValue(this, "post.TRASMEDITERRANEA"));
        boolean addocean1Post = Boolean.parseBoolean(Helper.getConfigValue(this, "post.ADDOCEAN_1"));
        boolean addocean2Post = Boolean.parseBoolean(Helper.getConfigValue(this, "post.ADDOCEAN_2"));
        boolean addocean3Post = Boolean.parseBoolean(Helper.getConfigValue(this, "post.ADDOCEAN_3"));

        post.put("AML", amlPost);
        post.put("BALEARIA", baleariaPost);
        post.put("FRS", frsPost);
        post.put("INTERSHIPPING", interShippingPost);
        post.put("TRASMEDITERRANEA", trasmediterraneaPost);
        post.put("ADDOCEAN_1", addocean1Post);
        post.put("ADDOCEAN_2", addocean2Post);
        post.put("ADDOCEAN_3", addocean3Post);

        ticketDataPost = new TicketDataPost();

        et1.addTextChangedListener(loginTextWatcher);

        View.OnClickListener sendButtonListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    StringBuilder resultString = new StringBuilder("0000");
                    Integer resultType = null;
                    if (!greenButton.getState()){
                        if (quantityButton.getState()){
                            resultString.setCharAt(0,'1');
                        }
                        if (vehicleButton.getState()){
                            resultString.setCharAt(1, '1');
                        }
                        if (ageButton.getState()){
                            resultString.setCharAt(2, '1');
                        }
                        if (boardingButton.getState()) {
                            resultString.setCharAt(3, '1');
                        }
                    }
                    resultType = Integer.parseInt(resultString.toString(), 2);
                    ticketDataPost.setPrecheckerResult(resultType);
                    sendPost(ticketDataPost);
                    getShippingLineStatus();
            }
        };

        View.OnClickListener resultButtonListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!selectedCompanyName.isEmpty() && !selectedNumber.isEmpty() && !selectedTicketType.isEmpty()) {

                    switch (view.getId()) {
                        case R.id.fab2:
                            quantityButton.setState(!quantityButton.getState());
                            greenButton.setState(false);
                            refreshSendButton();
                            refreshExpandableButton();
                            break;
                        case R.id.fab3:
                            greenButton.setState(!greenButton.getState());
                            if (expandableButton.getState()) {
                                quantityButton.setState(false);
                                ageButton.setState(false);
                                vehicleButton.setState(false);
                                boardingButton.setState(false);
                            }
                            refreshSendButton();
                            refreshExpandableButton();
                            if (expandableClicked) {
                                expandableClicked = false;
                                prepareExpandableButtons();
                            }
                            break;
                        case R.id.fab4:
                            vehicleButton.setState(!vehicleButton.getState());
                            greenButton.setState(false);
                            refreshSendButton();
                            refreshExpandableButton();
                            break;
                        case R.id.fab5:
                            ageButton.setState(!ageButton.getState());
                            greenButton.setState(false);
                            refreshSendButton();
                            refreshExpandableButton();
                            break;
                        case R.id.fab7:
                            boardingButton.setState(!boardingButton.getState());
                            greenButton.setState(false);
                            refreshSendButton();
                            refreshExpandableButton();
                            break;
                        case R.id.expandableButton:
                            expandableClicked = !expandableClicked;
                            prepareExpandableButtons();
                            break;
                        default:
                    }
                } else {
                    greenButton.setEnabled(false);
                    quantityButton.setEnabled(false);
                    vehicleButton.setEnabled(false);
                    ageButton.setEnabled(false);
                    sendButton.setEnabled(false);
                }
            }
        };


        FloatingActionButton floatingQuantityButton = findViewById(R.id.fab2);
        floatingQuantityButton.setOnClickListener(resultButtonListener);

        FloatingActionButton floatingGreenButton = findViewById(R.id.fab3);
        floatingGreenButton.setOnClickListener(resultButtonListener);

        FloatingActionButton floatingVehicleTypeButton = findViewById(R.id.fab4);
        floatingVehicleTypeButton.setOnClickListener(resultButtonListener);

        FloatingActionButton floatingAgeButton = findViewById(R.id.fab5);
        floatingAgeButton.setOnClickListener(resultButtonListener);

        FloatingActionButton floatingSendButton = findViewById(R.id.fab6);
        floatingSendButton.setOnClickListener(sendButtonListener);

        FloatingActionButton floatingBoardingButton = findViewById(R.id.fab7);
        floatingBoardingButton.setOnClickListener(resultButtonListener);

        FloatingActionButton floatingExpandableButton = findViewById(R.id.expandableButton);
        floatingExpandableButton.setOnClickListener(resultButtonListener);

        greenButton = new ToggleButton(floatingGreenButton);
        greenButton.setClickImage(R.drawable.checkclick);
        greenButton.setUnclickImage(R.drawable.checkunclick);
        greenButton.setDisableImageId(R.drawable.checkdisable);
        greenButton.refresh();

        quantityButton = new ToggleButton(floatingQuantityButton);
        quantityButton.setClickImage(R.drawable.quantityclick);
        quantityButton.setUnclickImage(R.drawable.quantityunclick);
        quantityButton.setDisableImageId(R.drawable.quantitydisable);
        quantityButton.refresh();

        vehicleButton = new ToggleButton(floatingVehicleTypeButton);
        vehicleButton.setClickImage(R.drawable.vehicletypeclick);
        vehicleButton.setUnclickImage(R.drawable.vehicletypeunclick);
        vehicleButton.setDisableImageId(R.drawable.vehicletypedisable);
        vehicleButton.refresh();

        ageButton = new ToggleButton(floatingAgeButton);
        ageButton.setClickImage(R.drawable.agebuttonclick);
        ageButton.setUnclickImage(R.drawable.agebuttonunclick);
        ageButton.setDisableImageId(R.drawable.agebuttondisable);
        ageButton.refresh();

        sendButton = new ToggleButton(floatingSendButton);
        sendButton.setClickImage(R.drawable.sendenable);
        sendButton.setUnclickImage(R.drawable.sendenable);
        sendButton.setDisableImageId(R.drawable.senddisable);
        sendButton.refresh();

        boardingButton = new ToggleButton(floatingBoardingButton);
        boardingButton.setClickImage(R.drawable.boardingclick);
        boardingButton.setUnclickImage(R.drawable.boardingunclick);
        boardingButton.setDisableImageId(R.drawable.boardingdisable);
        boardingButton.refresh();

        expandableButton = new ToggleButton(floatingExpandableButton);
        expandableButton.setClickImage(R.drawable.expandableclick);
        expandableButton.setUnclickImage(R.drawable.expandableunclick);
        expandableButton.setDisableImageId(R.drawable.expandabledisable);
        expandableButton.refresh();

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        getShippingLineStatus();

        //Cargar valores al girar el movil
        cargarValoresGirarMovil(savedInstanceState);

        precheckGrabbaDriver.initialize();

        init();

    }

    @Override
    protected void onResume()
    {
        super.onResume();

        Log.i("GrabbaSDK", "onResume");

        precheckGrabbaDriver.registreCallbacks();

    }

    public void setSelectedNumber(String selectedNumber) {
        this.selectedNumber = selectedNumber;
    }

    //Método para hacer logs
    public void appendLog(String text)
    {
        File logFile = new File(this.getFilesDir(), "log.file");
        if (!logFile.exists())
        {
            try
            {
                if(logFile.createNewFile()){
                    Log.i("Log created", "The log file was created");

                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();

            FileInputStream fis = new FileInputStream(logFile);
            byte[] byteArray = new byte[(int)logFile.length()];
            fis.read(byteArray);
            String data = new String(byteArray);
            String[] stringArray = data.split("\n");
            if (stringArray.length > 1000)
            {
                removeFirstLine(logFile.getPath());
            }
            buf.close();

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void cargarValoresGirarMovil(Bundle savedInstanceState){
        if (savedInstanceState != null) {
            et1.setText(savedInstanceState.getString("et1"));
            selectedNumber = savedInstanceState.getString("selectedNumber");
            selectedTicketType = savedInstanceState.getString("selectedTicketType");
            selectedCompanyName = savedInstanceState.getString("selectedCompanyName");
            if (!selectedCompanyName.isEmpty()) {
                et1.setEnabled(true);
            }
            if (selectedCompanyName.equals("AML")) {
                buttonIcon1.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorIcon));
            } else if (selectedCompanyName.equals("Balearia")) {
                buttonIcon2.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorIcon));
            } else if (selectedCompanyName.equals("FRS")) {
                buttonIcon3.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorIcon));
            } else if (selectedCompanyName.equals("InterShipping")) {
                buttonIcon4.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorIcon));
            } else if (selectedCompanyName.equals("Trasmediterranea")) {
                buttonIcon5.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorIcon));
            }
            resultTextView.setText(savedInstanceState.getString("resultTextView"));
            if (!resultTextView.getText().toString().isEmpty() && !resultTextView.getText().toString().equals(ticketNotFound))
            {
                resultLayout.setVisibility(View.VISIBLE);
                arrivalPortTanger = savedInstanceState.getBoolean(arrivalPortTangerString);
                applyApiResultToView(true, false);
                ticketDataPost.setCompany(CompanyEnum.valueOf(selectedCompanyName));
                ticketDataPost.setElementType(ElementTypeEnum.valueOf(selectedTicketType));
                ticketDataPost.setNumber(selectedNumber);
                ticketDataPost.setMilliesOfResponse(diffInMillies);
                greenButton.setState(savedInstanceState.getBoolean("greenButtonState"));
                expandableButton.setState(savedInstanceState.getBoolean("expandableButtonState"));
                quantityButton.setState(savedInstanceState.getBoolean("quantityButtonState"));
                vehicleButton.setState(savedInstanceState.getBoolean("vehicleTypeButtonState"));
                ageButton.setState(savedInstanceState.getBoolean("ageButtonState"));
                boardingButton.setState(savedInstanceState.getBoolean("boardingButtonState"));
                sendButton.setState(savedInstanceState.getBoolean("sendButtonState"));
                refreshSendButton();
            } else if (resultTextView.getText().toString().equals(ticketNotFound))
            {
                resultLayout.setVisibility(View.VISIBLE);
                arrivalPortTanger = savedInstanceState.getBoolean(arrivalPortTangerString);
                applyApiResultToView(false, false);
            }
        }
    }

    public static void removeFirstLine(String fileName) throws IOException {
        RandomAccessFile raf = new RandomAccessFile(fileName, "rw");
        //Initial write position
        long writePosition = raf.getFilePointer();
        raf.readLine();
        // Shift the next lines upwards.
        long readPosition = raf.getFilePointer();

        byte[] buff = new byte[1024];
        int n;
        while (-1 != (n = raf.read(buff))) {
            raf.seek(writePosition);
            raf.write(buff, 0, n);
            readPosition += n;
            writePosition += n;
            raf.seek(readPosition);
        }
        raf.setLength(writePosition);
        raf.close();
    }

    //Metodo para poner a todos los botones de color blanco
    public void blankButtons() {
        buttonIcon1.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.white));
        buttonIcon2.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.white));
        buttonIcon3.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.white));
        buttonIcon4.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.white));
        buttonIcon5.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.white));
    }

    public void refreshSendButton(){
        if (!greenButton.getState() && !quantityButton.getState() && !ageButton.getState() && !vehicleButton.getState() && !boardingButton.getState()){
            sendButton.setEnabled(false);
        } else {
            sendButton.setEnabled(true);
        }
    }

    public void refreshExpandableButton() {
        if (quantityButton.getState() || ageButton.getState() || vehicleButton.getState() || boardingButton.getState()) {
            expandableButton.setState(true);
        } else {
            expandableButton.setState(false);
        }
    }

    private void prepareExpandableButtons() {
        if (expandableClicked) {
            quantityButton.setVisibility(View.VISIBLE);
            ageButton.setVisibility(View.VISIBLE);
            vehicleButton.setVisibility(View.VISIBLE);
            boardingButton.setVisibility(View.VISIBLE);

            quantityButton.startAnimation(fromLeft);
            ageButton.startAnimation(fromLeft);
            vehicleButton.startAnimation(fromLeft);
            boardingButton.startAnimation(fromLeft);

            quantityButton.isClickable(true);
            ageButton.isClickable(true);
            vehicleButton.isClickable(true);
            boardingButton.isClickable(true);
        } else {
            quantityButton.setVisibility(View.INVISIBLE);
            ageButton.setVisibility(View.INVISIBLE);
            vehicleButton.setVisibility(View.INVISIBLE);
            boardingButton.setVisibility(View.INVISIBLE);

            quantityButton.startAnimation(toLeft);
            ageButton.startAnimation(toLeft);
            vehicleButton.startAnimation(toLeft);
            boardingButton.startAnimation(toLeft);

            quantityButton.isClickable(false);
            ageButton.isClickable(false);
            vehicleButton.isClickable(false);
            boardingButton.isClickable(false);
        }
    }

    //Metodo para los botones de las companias
    public void selectCompany(View view) {

        et1.setEnabled(true);
        String companyName = "";
        switch (view.getId()) {
            case R.id.button1:
                appendLog("Seleccionar la compañía AML");
                companyName = Helper.getConfigValue(this, "AML.name");
                selectedCompanyName = companyName;
                blankButtons();
                buttonIcon1.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorIcon));
                ticketDataPost.setCompany(CompanyEnum.AML);
                break;
            case R.id.button2:
                companyName = Helper.getConfigValue(this, "Balearia.name");
                selectedCompanyName = companyName;
                blankButtons();
                buttonIcon2.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorIcon));
                ticketDataPost.setCompany(CompanyEnum.BALEARIA);
                break;
            case R.id.button3:
                companyName = Helper.getConfigValue(this, "FRS.name");
                selectedCompanyName = companyName;
                blankButtons();
                buttonIcon3.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorIcon));
                ticketDataPost.setCompany(CompanyEnum.FRS);
                break;
            case R.id.button4:
                companyName = Helper.getConfigValue(this, "InterShipping.name");
                selectedCompanyName = companyName;
                blankButtons();
                buttonIcon4.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorIcon));
                ticketDataPost.setCompany(CompanyEnum.INTERSHIPPING);
                break;
            case R.id.button5:
                companyName = Helper.getConfigValue(this, "Trasmediterranea.name");
                selectedCompanyName = companyName;
                blankButtons();
                buttonIcon5.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorIcon));
                ticketDataPost.setCompany(CompanyEnum.TRASMEDITERRANEA);
                break;
            default:
                throw new RuntimeException("Unknow button ID");
        }
        selectedNumber = "";
        selectedTicketType = "";
        resultTextView.setText("");
        resultLayout.setVisibility(View.INVISIBLE);
        String res = et1.getText().toString().trim();
        buttonComp1.setEnabled(!res.isEmpty());
        buttonComp2.setEnabled(!res.isEmpty());
        buttonComp3.setEnabled(!res.isEmpty());
        applyApiResultToView(false, true);
        if (expandableClicked) {
            expandableClicked = false;
            prepareExpandableButtons();
        }
        refreshSendButton();
    }

    //Habilitar un boton cuando se escribe un ticket
    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //Método sin usar
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String res = et1.getText().toString().trim();
            if (!selectedCompanyName.isEmpty()) {
                buttonComp1.setEnabled(!res.isEmpty());
                buttonComp2.setEnabled(!res.isEmpty());
                buttonComp3.setEnabled(!res.isEmpty());
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            //Método sin usar
        }
    };

    //Metodo para escoger el tipo de ticket
    public void selectTicketType(View view) {
        et1.onEditorAction(EditorInfo.IME_ACTION_DONE);

        buttonComp1.setBackground(ContextCompat.getDrawable(this, view.getId() == buttonComp1.getId() ? R.drawable.button_border_selected : R.drawable.button_border));
        buttonComp2.setBackground(ContextCompat.getDrawable(this, view.getId() == buttonComp2.getId() ? R.drawable.button_border_selected : R.drawable.button_border));
        buttonComp3.setBackground(ContextCompat.getDrawable(this, view.getId() == buttonComp3.getId() ? R.drawable.button_border_selected : R.drawable.button_border));

        String resized = String.format("%15s", et1.getText().toString());
        selectedNumber = resized.replace(" ", "0");
        switch (view.getId()) {
            case R.id.button6:
                selectedTicketType = "BOOKING";
                ticketDataPost.setNumber(selectedNumber);
                ticketDataPost.setElementType(ElementTypeEnum.BOOKING);
                break;
            case R.id.button7:
                selectedTicketType = "TICKET";
                ticketDataPost.setNumber(selectedNumber);
                ticketDataPost.setElementType(ElementTypeEnum.TICKET);
                break;
            case R.id.button8:
                selectedTicketType = "BOARDING";
                ticketDataPost.setNumber(selectedNumber);
                ticketDataPost.setElementType(ElementTypeEnum.BOARDING);
                break;
            default:
                throw new RuntimeException("Unknow button ID");
        }
        doGetOrPost(selectedCompanyName);
    }

    private void doGetOrPost(String company) {
        if (selectedNumber.isEmpty() || selectedCompanyName.isEmpty() || selectedTicketType.isEmpty())
        {
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);
        progressBar.setActivated(true);
        resultLayout.setVisibility(View.INVISIBLE);

        boolean isBooking = selectedTicketType.equalsIgnoreCase("Booking");
        String bookingNumber = isBooking ? selectedNumber : "000000000000000";
        String ticketNumber = !isBooking ? selectedNumber : "000000000000000";
        String departurePort = "ESALG";

        String apiUrl = Helper.getConfigValue(this, selectedCompanyName);
        final String token = "Basic " + Helper.getConfigValue(this, "token." + selectedCompanyName);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request originalRequest = chain.request();

                        Request newRequest = originalRequest.newBuilder()
                                .header("Authorization", token)
                                .header("Content-Type", "application/json")
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        JsonAddOceanApi jsonAddOceanApi = retrofit.create(JsonAddOceanApi.class);

        boolean doPost = post.get(company.toUpperCase());
        Call<TicketData[]> call;
        if (doPost) {
            beforeAnswer = new Date();
            call = jsonAddOceanApi.postTicketData(Helper.getConfigValue(this, "api." + selectedCompanyName), bookingNumber, ticketNumber, departurePort);
        } else {
            beforeAnswer = new Date();
            call = jsonAddOceanApi.getTicketData(Helper.getConfigValue(this, "api." + selectedCompanyName), bookingNumber, ticketNumber, departurePort);
        }
        getResults(call);

    }

    private void getResults(Call<TicketData[]> call){
        call.enqueue(new Callback<TicketData[]>() {
            @Override
            public void onResponse(Call<TicketData[]> call, Response<TicketData[]> response) {
                String textToShow;
                System.out.println(response.body());
                if (!response.isSuccessful()) {
                    textToShow = ticketNotFound;
                    sendButton.setEnabled(false);
                    if (expandableClicked) {
                        expandableClicked = false;
                        prepareExpandableButtons();
                    }
                } else {
                    TicketData[] tickets = response.body();
                    textToShow = getInfoTextFromTickets(tickets);
                }

                resultTextView.setText(textToShow);
                resultLayout.setVisibility(View.VISIBLE);
                afterAnswer = new Date();
                diffInMillies = afterAnswer.getTime() - beforeAnswer.getTime();
                ticketDataPost.setMilliesOfResponse(diffInMillies);
                //Toast.makeText(MainActivity.this, String.valueOf(diffInMillies), Toast.LENGTH_SHORT).show();
                applyApiResultToView(response.isSuccessful(), false);
                progressBar.setVisibility(View.INVISIBLE);
                progressBar.setActivated(false);
            }

            @Override
            public void onFailure(Call<TicketData[]> call, Throwable t) {
                System.out.println("joder");
                System.out.println(t.getMessage());
                TextView mJsonTextFailure = new TextView(MainActivity.this);
                mJsonTextFailure.setText(t.getMessage());
                mJsonTextFailure.setTextSize(18);
                progressBar.setVisibility(View.INVISIBLE);
                progressBar.setActivated(false);
            }
        });
    }

    private String getInfoTextFromTickets(TicketData[] tickets) {
        StringBuilder content = new StringBuilder();
        int adults = 0;
        int children = 0;
        int babies = 0;
        boolean firstVehicle = true;

        if (tickets.length > 0) {
            arrivalPortTanger = !arrivalPortTanger;
            String arrivalPortString = arrivalPortTanger ? "Tánger Med" : "Ceuta";
            content.append("Ticket to: " + arrivalPortString + "\n");
        }

        for (TicketData ticketData : tickets) {
            for (Passenger passenger : ticketData.getBookingInfoData().getPassengers()) {
                if (passenger.getAgeCategory().equals("1")) {
                    adults++;
                } else if (passenger.getAgeCategory().equals("2")) {
                    children++;
                } else if (passenger.getAgeCategory().equals("3")) {
                    babies++;
                }
            }
            for (Vehicle vehicle : ticketData.getBookingInfoData().getVehicles()) {
                if (firstVehicle) {
                    if (ticketData.getVesselName().isEmpty() && ticketData.getDepartureDate().isEmpty() && ticketData.getDepartureTime().isEmpty()) {
                        content.append("Embarque abierto \n");
                        boardingButton.setEnabled(false);
                        boardingButton.refresh();
                    } else {
                        content.append("Buque: " + ticketData.getVesselName() + "\n");
                        content.append("Fecha: " + ticketData.getDepartureTime() + " " + ticketData.getDepartureDate() + "\n");
                        boardingButton.setEnabled(true); 
                        boardingButton.refresh();
                    }
                    content.append("Vehicles:\n");
                    firstVehicle = false;
                }
                content.append(vehicle.getRegistrationNumber() + "\t\t");
                content.append(vehicle.getBrand() + "\t\t");
                content.append(vehicle.getModel() + "\t\t");
                content.append("(" + vehicle.getVehicleType() + ")\n");
            }
        }

        if (adults > 0 || children > 0 || babies > 0) {
            content.append("Passengers:\n");
        }
        content.append("Adults = " + adults + "\n");
        content.append("Children = " + children + "\n");
        content.append("Babies = " + babies);



        return content.toString();
    }

    private void applyApiResultToView(boolean hasValidResult, boolean reset) {
        quantityButton.setEnabled(hasValidResult && !reset);
        greenButton.setEnabled(hasValidResult && !reset);
        vehicleButton.setEnabled(hasValidResult && !reset);
        ageButton.setEnabled(hasValidResult && !reset);
        expandableButton.setEnabled(hasValidResult && !reset);

        greenButton.refresh();
        quantityButton.refresh();
        vehicleButton.refresh();
        ageButton.refresh();
        expandableButton.refresh();


        int drawableResource = R.drawable.tickets_back_none;
        if (!reset && hasValidResult && arrivalPortTanger)
        {
            drawableResource = R.drawable.tickets_back_tanger;
        }
        else if (!reset && hasValidResult && !arrivalPortTanger)
        {
            drawableResource = R.drawable.tickets_back_ceuta;
        }
        resultLayout.setBackground(ContextCompat.getDrawable(this, drawableResource));
    }

    private void sendPost(TicketDataPost ticketDataPost) {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setActivated(true);

        mAPIService.postTicketData(ticketDataPost).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Submited post to API", Toast.LENGTH_SHORT).show();
                    selectedCompanyName = "";
                    selectedNumber = "";
                    selectedTicketType = "";
                    resultTextView.setText("");
                    buttonComp1.setEnabled(false);
                    buttonComp2.setEnabled(false);
                    buttonComp3.setEnabled(false);
                    resultLayout.setVisibility(View.INVISIBLE);
                    et1.setText("");
                    et1.setEnabled(false);
                    blankButtons();
                    resetTicketTypeButtons();
                    applyApiResultToView(false, true);
                    refreshSendButton();
                    if (expandableClicked) {
                        expandableClicked = false;
                        prepareExpandableButtons();
                    }
                    refreshExpandableButton();
                }

                progressBar.setVisibility(View.INVISIBLE);
                progressBar.setActivated(false);
                try {
                    Log.d("RetrofitExample", response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Fail to submit post to API", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getShippingLineStatus(){
        Call<ShippingLineStatus> call = mAPIService.getShippingLineStatus();
        call.enqueue(new Callback<ShippingLineStatus>() {

            @Override
            public void onResponse(Call<ShippingLineStatus> call, Response<ShippingLineStatus> response) {
                if (response.isSuccessful()){
                    ShippingLineStatus shippingLineStatus = response.body();
                    refreshCompanyButtonsStatus(shippingLineStatus);
                }
            }

            @Override
            public void onFailure(Call<ShippingLineStatus> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Fail to get shipping line status", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void refreshCompanyButtonsStatus(ShippingLineStatus shippingLineStatus){
        buttonIcon1.setEnabled(shippingLineStatus.getAmlStatus());
        buttonIcon2.setEnabled(shippingLineStatus.getBaleariaStatus());
        buttonIcon3.setEnabled(shippingLineStatus.getFrsStatus());
        buttonIcon4.setEnabled(shippingLineStatus.getIntershippingStatus());
        buttonIcon5.setEnabled(shippingLineStatus.getTrasmediterraneaStatus());

        blankButtons();

        buttonIcon1.setForeground(ContextCompat.getDrawable(this, shippingLineStatus.getAmlStatus() ? R.drawable.aml_green : R.drawable.aml_red));
        buttonIcon2.setForeground(ContextCompat.getDrawable(this, shippingLineStatus.getBaleariaStatus() ? R.drawable.balearia_green : R.drawable.balearia_red));
        buttonIcon3.setForeground(ContextCompat.getDrawable(this, shippingLineStatus.getFrsStatus() ? R.drawable.frs_green : R.drawable.frs_red));
        buttonIcon4.setForeground(ContextCompat.getDrawable(this, shippingLineStatus.getIntershippingStatus() ? R.drawable.intershipping_green : R.drawable.intershipping_red));
        buttonIcon5.setForeground(ContextCompat.getDrawable(this, shippingLineStatus.getTrasmediterraneaStatus() ? R.drawable.trasmediterranea_green : R.drawable.trasmediterranea_red));
    }

    private void resetTicketTypeButtons() {
        buttonComp1.setBackground(ContextCompat.getDrawable(this, R.drawable.button_border));
        buttonComp2.setBackground(ContextCompat.getDrawable(this, R.drawable.button_border));
        buttonComp3.setBackground(ContextCompat.getDrawable(this, R.drawable.button_border));
    }

    //Metodos para guardar el estado de las variables cuando se gira el movil
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("et1", et1.getText().toString());
        outState.putInt("colorButtonIcon1", buttonIcon1.getBackgroundTintList().getDefaultColor());
        outState.putInt("colorButtonIcon2", buttonIcon2.getBackgroundTintList().getDefaultColor());
        outState.putInt("colorButtonIcon3", buttonIcon3.getBackgroundTintList().getDefaultColor());
        outState.putInt("colorButtonIcon4", buttonIcon4.getBackgroundTintList().getDefaultColor());
        outState.putInt("colorButtonIcon5", buttonIcon5.getBackgroundTintList().getDefaultColor());
        outState.putString("selectedCompanyName", selectedCompanyName);
        outState.putString("selectedTicketType", selectedTicketType);
        outState.putString("selectedNumber", selectedNumber);
        outState.putString("resultTextView", resultTextView.getText().toString());
        outState.putBoolean(arrivalPortTangerString, arrivalPortTanger);
        outState.putBoolean("greenButtonState", greenButton.getState());
        outState.putBoolean("expandableButtonState", expandableButton.getState());
        outState.putBoolean("quantityButtonState", quantityButton.getState());
        outState.putBoolean("vehicleTypeButtonState", vehicleButton.getState());
        outState.putBoolean("ageButtonState", ageButton.getState());
        outState.putBoolean("boardingButtonState", boardingButton.getState());
        outState.putBoolean("sendButtonState", sendButton.getState());
    }


    private void setText(final int id, String text)
    {
        final String t = text;
        runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                try
                {
                    appendLog("Intentando setear el texto " + t);
                    ((TextView) findViewById(id)).setText(t);
                }
                catch (NullPointerException e)
                {
                    e.printStackTrace();
                }
                appendLog("Texto seteado");
            }
        });
    }

    private void init()
    {
        // Hook up scan and clear buttons
        findViewById(R.id.btnScan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                // you cannot call the grabba barcode function from the UI thread
                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params)
                    {
                        precheckGrabbaDriver.scanNewNumber();
                        return null;
                    }
                }.execute();
            }
        });

    }

    public void setGrabbaStatus(int id, String grabbaStatusText) {
        setText(id, grabbaStatusText);
    }
}
