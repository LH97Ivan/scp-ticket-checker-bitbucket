package com.example.scpticketchecker;

import com.grabba.Grabba;
import com.grabba.GrabbaBarcode;
import com.grabba.GrabbaBarcodeListener;
import com.grabba.GrabbaButtonListener;
import com.grabba.GrabbaConnectionListener;
import com.grabba.GrabbaDriverNotInstalledException;
import com.grabba.GrabbaException;

class PrecheckGrabbaDriver {
    private final MainActivity mainActivity;
    private boolean initialize;

    public PrecheckGrabbaDriver(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        initialize = false;
    }

    public void initialize() {
        try
        {
            Grabba.open(mainActivity, "GrabbaBarcodeSample");
            initialize = true;
        }
        catch (GrabbaDriverNotInstalledException e)
        {
            // setText(R.id.txtStatus, e.toString());
        }
    }

    public void registreCallbacks() {
        if (initialize)
        {
            // Register for Grabba connection events
            Grabba.getInstance().addConnectionListener(grabbaConnectionListener);

            // Register for barcode events
            GrabbaBarcode.getInstance().addEventListener(grabbaBarcodeListener);

            // Register for Grabba button events
            Grabba.getInstance().addButtonListener(grabbaButtonListener);

            Grabba.getInstance().acquireGrabba();
        }
    }

    private final GrabbaConnectionListener grabbaConnectionListener = new GrabbaConnectionListener() {

        @Override
        public void grabbaDisconnectedEvent()
        {
            mainActivity.appendLog("Grabba desconectado");
            mainActivity.setGrabbaStatus (R.id.txtStatus, "Grabba Disconected");
        }

        @Override
        public void grabbaConnectedEvent()
        {
            mainActivity.appendLog("Grabba conectado");
            mainActivity.setGrabbaStatus(R.id.txtStatus,"Grabba Connected");
        }
    };

    private final GrabbaButtonListener grabbaButtonListener = new GrabbaButtonListener() {
        @Override
        public void grabbaRightButtonEvent(boolean pressed)
        {
            if (pressed)
            {
                try
                {
                    // The button events don't fire on the UI thread so we can safely call the Grabba barcode trigger function
                    GrabbaBarcode.getInstance().trigger(true);
                }
                catch (GrabbaException e)
                {
                    // setText(R.id.txtStatus, e.toString());
                }
            }
        }

        @Override
        public void grabbaLeftButtonEvent(boolean pressed)
        {
            if (pressed)
            {
                try
                {
                    // The button events don't fire on the UI thread so we can safely call the Grabba barcode trigger function
                    GrabbaBarcode.getInstance().trigger(true);
                }
                catch (GrabbaException e)
                {
                    // setText(R.id.txtStatus, e.toString());
                }
            }
        }
    };

    private final GrabbaBarcodeListener grabbaBarcodeListener = new GrabbaBarcodeListener() {

        @Override
        public void barcodeTriggeredEvent()
        {
            mainActivity.appendLog("Evento del lector de código de barras");
            mainActivity.setGrabbaStatus(R.id.txtStatus,"");
        }

        @Override
        public void barcodeTimeoutEvent()
        {
            mainActivity.appendLog("Timeout del lector de código de barras");
            mainActivity.setGrabbaStatus(R.id.txtStatus,"");
        }

        @Override
        public void barcodeScanningStopped()
        {
            mainActivity.appendLog("Lector de código de barras detenido");
            mainActivity.setGrabbaStatus(R.id.txtStatus,"");
        }

        @Override
        public void barcodeScannedEvent(String barcode, int symbologyType)
        {
            mainActivity.setGrabbaStatus(R.id.txtStatus,"");
            mainActivity.setSelectedNumber(barcode);
            mainActivity.setGrabbaStatus(R.id.editText1, barcode);
            mainActivity.appendLog("Lector de código de barras escaneado");
        }
    };

    public void scanNewNumber() {
        try
        {
            // Start barcode scanning
            GrabbaBarcode.getInstance().trigger(true);
        }
        catch (GrabbaException e)
        {
            // Print the exception on the screen
            // setText(R.id.txtStatus, e.toString());
        }
    }
}
