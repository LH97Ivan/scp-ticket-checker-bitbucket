package Interface;

import com.example.scpticketchecker.model.request.ShippingLineStatus;
import com.example.scpticketchecker.model.request.TicketData;
import com.example.scpticketchecker.model.response.TicketDataPost;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;


public interface JsonAddOceanApi {

    @GET
    Call<TicketData[]> getTicketData(@Url String apiCompany, @Query("bookingNumber") String bookingNumber, @Query("ticketNumber") String ticketNumber, @Query("departurePort") String departurePort);

    @POST
    Call<TicketData[]> postTicketData(@Url String apiCompany, @Query("bookingNumber") String bookingNumber, @Query("ticketNumber") String ticketNumber, @Query("departurePort") String departurePort);

    @GET("/shipping/result/")
    Call<ShippingLineStatus> getShippingLineStatus();

    @POST("/prechecker/result/")
    Call<ResponseBody> postTicketData(@Body TicketDataPost ticketDataPost);
}
